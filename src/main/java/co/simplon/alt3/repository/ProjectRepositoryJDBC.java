package co.simplon.alt3.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.alt3.entity.Project;
import co.simplon.alt3.entity.proxy.StudentProxy;

public class ProjectRepositoryJDBC implements IRepository<Project> {

    @Override
    public List<Project> findAll() {
        List<Project> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM project");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToProject(rs));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Project findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM project WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToProject(rs);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    private Project sqlToProject(ResultSet rs) throws SQLException {
        Project project = new Project(
                rs.getInt("id"),
                rs.getString("techs"),
                rs.getString("name"),
                rs.getString("picture"));
        project.setStudent(
            new StudentProxy(rs.getInt("id_student"))
        );
        return project;
    }

}
