package co.simplon.alt3.repository.decorator;

import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;

public abstract class RepoDecorator implements IRepository<Student> {

    protected IRepository<Student> repo;

    public RepoDecorator(IRepository<Student> repo) {
        this.repo = repo;
    }

}
