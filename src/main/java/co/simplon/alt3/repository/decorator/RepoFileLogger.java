package co.simplon.alt3.repository.decorator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;

import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;

public class RepoFileLogger extends RepoDecorator {

    private Path logPath = Paths.get("repo.logs");

    public RepoFileLogger(IRepository<Student> repo) {
        super(repo);

    }

    @Override
    public List<Student> findAll() {
        try {
            Files.writeString(logPath,LocalDateTime.now() + " : BEGIN STUDENT FIND ALL \n", StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Student> result = repo.findAll();
        try {
            Files.writeString(logPath,LocalDateTime.now() + " : END STUDENT FIND ALL \n", StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Student findById(int id) {
        // TODO Auto-generated method stub
        return null;
    }
}
