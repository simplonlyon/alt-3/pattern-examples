package co.simplon.alt3.repository.decorator;

import java.util.List;

import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;

public class CachingRepoDecorator extends RepoDecorator {
    private static List<Student> cache = null;
    private static boolean stale = true;

    public CachingRepoDecorator(IRepository<Student> repo) {
        super(repo);
    }

    @Override
    public List<Student> findAll() {
        if(cache == null || stale) {
            cache = repo.findAll();
            stale = false;
        }
        return cache;
    }

    @Override
    public Student findById(int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
