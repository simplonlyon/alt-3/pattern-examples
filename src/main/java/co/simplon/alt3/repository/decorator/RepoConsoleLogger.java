package co.simplon.alt3.repository.decorator;

import java.time.LocalDateTime;
import java.util.List;

import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;

public class RepoConsoleLogger extends RepoDecorator {

    @Override
    public Student findById(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    public RepoConsoleLogger(IRepository<Student> repo) {
        super(repo);
    }

    @Override
    public List<Student> findAll() {
        System.out.println(LocalDateTime.now() + " : BEGIN STUDENT FIND ALL");
        List<Student> result = repo.findAll();
        System.out.println(LocalDateTime.now() + " : END STUDENT FIND ALL");
        return result;
    }
    
}
