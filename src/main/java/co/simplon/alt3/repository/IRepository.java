package co.simplon.alt3.repository;

import java.util.List;

public interface IRepository<T> {
    List<T> findAll();
    T findById(int id);
}
