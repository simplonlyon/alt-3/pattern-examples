package co.simplon.alt3.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.alt3.entity.Student;

public class StudentRepositoryJDBC implements IRepository<Student>{

    @Override
    public List<Student> findAll() {
        List<Student> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                list.add(sqlToStudent(rs));
            }

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return list;
    }


    @Override
    public Student findById(int id) { 
        try (Connection connection = DbConnect.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student WHERE id=?");
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();

        if(rs.next()) {
            return sqlToStudent(rs);
        }

    } catch (SQLException e) {
        
        e.printStackTrace();
    }
        return null;
    }
    
    private Student sqlToStudent(ResultSet rs) throws SQLException {
        return new Student(
            rs.getInt("id"), 
            rs.getString("mail"), 
            rs.getString("name"), 
            rs.getString("session")
        );
    }
}
