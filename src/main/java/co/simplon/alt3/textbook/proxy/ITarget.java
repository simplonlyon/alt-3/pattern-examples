package co.simplon.alt3.textbook.proxy;

public interface ITarget {
    String doStuff();
}
