package co.simplon.alt3.textbook.proxy;

public class ProxyTarget implements ITarget {
    private ITarget instance;

    @Override
    public String doStuff() {
        if(instance == null) {
            instance = new TargetImplementation();
        }
        return instance.doStuff();
    }
    
    

}
