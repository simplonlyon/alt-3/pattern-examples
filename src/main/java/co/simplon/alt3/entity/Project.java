package co.simplon.alt3.entity;

public class Project {
    private Integer id;
    private String techs;
    private String name;
    private String picture;
    private Student student;
    
    public Project() {
    }
    public Project(String techs, String name, String picture) {
        this.techs = techs;
        this.name = name;
        this.picture = picture;
    }
    public Project(Integer id, String techs, String name, String picture) {
        this.id = id;
        this.techs = techs;
        this.name = name;
        this.picture = picture;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTechs() {
        return techs;
    }
    public void setTechs(String techs) {
        this.techs = techs;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public Student getStudent() {
        return student;
    }
    public void setStudent(Student student) {
        this.student = student;
    }
}
