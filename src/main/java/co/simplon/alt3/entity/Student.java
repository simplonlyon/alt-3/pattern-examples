package co.simplon.alt3.entity;

public class Student {
    private Integer id;
    private String mail;
    private String name;
    private String session;
    public Student(String mail, String name, String session) {
        this.mail = mail;
        this.name = name;
        this.session = session;
    }
    public Student() {
    }
    public Student(Integer id, String mail, String name, String session) {
        this.id = id;
        this.mail = mail;
        this.name = name;
        this.session = session;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSession() {
        return session;
    }
    public void setSession(String session) {
        this.session = session;
    }
}
