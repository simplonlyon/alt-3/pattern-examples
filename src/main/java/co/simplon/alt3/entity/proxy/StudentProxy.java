package co.simplon.alt3.entity.proxy;

import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;
import co.simplon.alt3.repository.StudentRepositoryJDBC;

public class StudentProxy extends Student {
    private Student instance;
    private IRepository<Student> repo = new StudentRepositoryJDBC();

    public StudentProxy(Integer id) {
        setId(id);

    }

    private void loadStudent() {
        if(instance == null){
            System.out.println("Loading Student...");
            instance = repo.findById(getId());
        }
    }

    @Override
    public String getMail() {
        loadStudent();
        return instance.getMail();
    }

    @Override
    public String getName() {
        loadStudent();
        return instance.getName();
    }

    @Override
    public String getSession() {
        loadStudent();
        return instance.getSession();
    }

    
    
}
