package co.simplon.alt3;

import java.util.List;

import co.simplon.alt3.entity.Project;
import co.simplon.alt3.entity.Student;
import co.simplon.alt3.repository.IRepository;
import co.simplon.alt3.repository.ProjectRepositoryJDBC;
import co.simplon.alt3.repository.StudentRepositoryJDBC;
import co.simplon.alt3.repository.decorator.CachingRepoDecorator;
import co.simplon.alt3.repository.decorator.RepoConsoleLogger;
import co.simplon.alt3.repository.decorator.RepoFileLogger;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // IRepository<Student> repo = createRepository(true, true,true);
        // System.out.println(repo.findAll());

        IRepository<Project> repro = new ProjectRepositoryJDBC();

        List<Project> projects = repro.findAll();

        for (Project project : projects) {
            System.out.println(project.getStudent());
        }

    }

    /**
     * Ptite méthode factory, à la fraîche
     * @param withLogger
     * @param withFile
     * @param withCache
     * @return
     */
    public static IRepository<Student> createRepository(boolean withLogger, boolean withFile, boolean withCache) {
        IRepository<Student> repo = new StudentRepositoryJDBC();
        if(withCache) {
            repo = new CachingRepoDecorator(repo);
        }
        if(withFile) {
            repo = new RepoFileLogger(repo);
        }
        if(withLogger) {
            repo = new RepoConsoleLogger(repo);
        }
        return repo;
    }
}
